FROM golang:latest

RUN mkdir /app

COPY . /app

RUN go get -u -v github.com/gorilla/mux

WORKDIR /app

RUN go build -o main .

EXPOSE 8000

CMD ["/app/main"]