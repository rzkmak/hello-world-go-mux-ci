package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Hello World V3!")
	})
	r.HandleFunc("/new-route", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "This is New Route to check CI/CD Pipelines")
	})
	print("Starting a server in 8000")
	log.Fatal(http.ListenAndServe(":8000", r))
}
