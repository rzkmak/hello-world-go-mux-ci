#!/bin/sh

IMAGE_ID=$(docker image ls | grep hello-world-go-mux-ci | awk '{print $3 "\t"}')

if ["" -eq "$IMAGE_ID"] 
  then echo "nothing to delete"
  else echo $DEV_PASSWORD | sudo -S docker image rm $IMAGE_ID
fi